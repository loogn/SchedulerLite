﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLite.Common
{
    public static class Config
    {
        /// <summary>
        /// 加载定时任务的间隔秒数，0为不启用
        /// </summary>
        public static int LoadTimedTaskIntervalSeconds
        {
            get
            {
                var loadTimedTaskIntervalSeconds = ConfigHelper.AppSetting("loadTimedTaskIntervalSeconds");
                if (string.IsNullOrEmpty(loadTimedTaskIntervalSeconds))
                {
                    return 0;
                }
                else
                {
                    return int.Parse(loadTimedTaskIntervalSeconds);
                }
            }
        }

        /// <summary>
        /// 加载延迟任务的间隔秒数，0为不启用
        /// </summary>
        public static int LoadDelayTaskIntervalSeconds
        {
            get
            {
                var loadDelayTaskIntervalSeconds = ConfigHelper.AppSetting("loadDelayTaskIntervalSeconds");
                if (string.IsNullOrEmpty(loadDelayTaskIntervalSeconds))
                {
                    return 0;
                }
                else
                {
                    return int.Parse(loadDelayTaskIntervalSeconds);
                }
            }
        }

        /// <summary>
        /// 延迟任务时间轮大小
        /// </summary>
        public static int TimeWheelSlotNum
        {
            get
            {
                var timeWheelSlotNum = ConfigHelper.AppSetting("timeWheelSlotNum");
                if (string.IsNullOrEmpty(timeWheelSlotNum))
                {
                    return 360;
                }
                else
                {
                    return int.Parse(timeWheelSlotNum);
                }
            }
        }

        /// <summary>
        /// 是否记录调用成功的日志
        /// </summary>
        public static bool RecordSuccessLog
        {
            get
            {
                var recordSuccessLog = ConfigHelper.AppSetting("recordSuccessLog");
                return "1".Equals(recordSuccessLog);
            }
        }
    }
}
