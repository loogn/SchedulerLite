﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulerLite.Model
{
    public class RunTaskResult
    {
        /// <summary>
        /// 状态，1成功，2失败，3超时
        /// </summary>
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
