﻿using SchedulerLite.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SchedulerLite.DAL
{
    public class DB
    {
        static string connStr;
        static DB()
        {
            connStr = ConfigHelper.ConnString("SchedulerLite");
        }

        public static IDbConnection Open()
        {
            return new SqlConnection(connStr);
        }
    }
}
